﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class test : MonoBehaviour {

    public SkinnedMeshRenderer[] Faces;
    private List<string> blend_List;
    private List<string> results_List;
    public float value1;
    public float value2;
    public float value3;
    private int id = 0;

    // Use this for initialization
    void Start()
    {
        blend_List = this.readFromFileToStrList("Book1.csv");
        results_List = this.readFromFileToStrList("results.csv");
        InvokeRepeating("LaunchProjectile", 2.0f, 0.1f);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public List<string> readFromFileToStrList(string fileNameWithExt)
    {
        List<string> strList = new List<string>();
        StreamReader sr = new StreamReader(Application.streamingAssetsPath + "/" + fileNameWithExt);
        string line = sr.ReadLine();
        while (line != null)
        {
            strList.Add(line);
            line = sr.ReadLine();
        }
        sr.Close();
        return strList;

    }

    private void setBlendLine(string s)
    {
        print(s);

        string line = s;
        string[] blend_values = line.Split(',');
        for (int i = 1; i < blend_values.Length -1; i++)
        {
            float v = float.Parse(blend_values[i]);
            //print(i);
            //print(float.Parse(blend_values[i]));
            SetBlend(i, v);
        }
    }

    void LaunchProjectile()
    {
        //print(id);
        setBlendLine(blend_List[id + 1]);
        (id < blend_List.Count - 1) ? id = id++ : id = 0;
       
        //SetBlend(0, value1);
        //SetBlend(1, value2);
        //SetBlend(18, value3);
        //value1 = (value1 + 10) % 100;
        //value2 = (value2 + 10) % 100;
        //value3 = (value3 + 10) % 100;
    }

    public void SetBlend(int shapeIndex,float weight)
    {
        foreach (SkinnedMeshRenderer Face in Faces)
        {
            Face.SetBlendShapeWeight(shapeIndex, weight);
        }
    }


    public void SetBlendValueBySliderValue(Slider slider)
    {
        SetBlend(slider.GetComponent<BlendIndex>().Index, slider.value * 100);
    }

}
